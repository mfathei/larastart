<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Image;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('isAdmin');

        return User::latest()->paginate(config('setting.paginate'));
    }

    public function profile()
    {
        return auth('api')->user();
    }

    public function updateProfile(Request $request)
    {
        $this->authorize('isAdmin');

        $name = '';
        $user = auth('api')->user();
        $currentPhoto = $user->photo;
        $path = public_path('img/profile/');
        $this->validate($request, [
            'name' => ['required', 'min:6', 'max:191'],
            'email' => ['required', 'email', 'max:191', 'unique:users,id,'.$user->id],
            'password' => ['sometimes', 'min:6', 'max:191'],
        ]);

        if ($request->photo != $currentPhoto) {
            $name = time().'.'.explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];
            Image::make($request->photo)->save($path.$name);
            $request->merge(['photo' => $name]);
        }

        if (! empty($request->password)) {
            $request->merge([
                'password' => Hash::make($request->password),
            ]);
        }

        $user->update($request->all());

        if ($request->photo != $currentPhoto) {
            if (file_exists($path.$currentPhoto)) {
                @unlink($path.$currentPhoto);
            }
        }

        return ['message' => 'Profile updated successfully'];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('isAdmin');

        $this->validate($request, [
            'name' => ['required', 'min:6', 'max:191'],
            'email' => ['required', 'email', 'max:191', 'unique:users'],
            'password' => ['required', 'min:6', 'max:191'],
        ]);

        return User::create([
            'name' => $request->name,
            'email' => $request->email,
            'type' => $request->type,
            'password' => Hash::make($request->password),
            'bio' => $request->bio,
            // 'photo' => $request->photo,
        ])->fresh();
    }

    /**
     * Display the specified resource.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => ['required', 'min:6', 'max:191'],
            'email' => ['required', 'email', 'max:191', 'unique:users,email,'.$id],
            'password' => ['sometimes', 'min:6', 'max:191'],
        ]);

        $user = User::findOrFail($id);
        $user->update($request->all());

        return ['message' => 'User updated successfully'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int                       $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');
        $user = User::findOrFail($id);
        $user->delete();

        return ['message' => 'User has been deleted successsfully.'];
    }

    public function search()
    {
        if ($search = request('q')) {
            return User::query()->where('name', 'like', "%$search%")
            ->orWhere('email', 'like', "%$search%")
            ->orWhere('type', 'like', "%$search%")
            ->paginate(config('setting.paginate'));
        }

        return User::paginate(config('setting.paginate'));
    }
}
