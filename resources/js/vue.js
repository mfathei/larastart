window.Vue = require('vue');
import VueRouter from 'vue-router';
import {
    Form,
    HasError,
    AlertError
} from 'vform';

import Gate from './gate';
Vue.prototype.$gate = new Gate(window.user);

/** ProgressBar */
import VueProgressBar from 'vue-progressbar'
Vue.use(VueProgressBar, {
    color: 'rgb(43, 255, 199)',
    failedColor: 'red',
    height: '5px'
});

/** router */
window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
/* passport */
Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);

Vue.component(
    'not-found',
    require('./components/NotFoundComponent.vue').default
);

Vue.component('pagination', require('laravel-vue-pagination'));
// ======================================================================
Vue.use(VueRouter);

import UsersComponent from './components/Users.vue';
import DashboardComponent from './components/Dashboard.vue';
import DeveloperComponent from './components/Developer.vue';
import ProfileComponent from './components/Profile.vue';
import NotFoundComponent from './components/NotFoundComponent.vue';
import moment from 'moment';

const routes = [{
        path: '/dashboard',
        component: DashboardComponent
    },
    {
        path: '/developer',
        component: DeveloperComponent
    },
    {
        path: '/users',
        component: UsersComponent
    },
    {
        path: '/profile',
        component: ProfileComponent
    },
    {
        path: '*',
        component: NotFoundComponent
    }
];

const router = new VueRouter({
    mode: 'history',
    routes
});

/** filters */

Vue.filter('upText', function ($value) {
    return $value ? $value.charAt(0).toUpperCase() + $value.slice(1) : '';
});

Vue.filter('myDate', function ($value) {
    return moment($value).format('MMMM Do YYYY');
});

// events
window.Fire = new Vue();


/** app */
const app = new Vue({
    el: '#app',
    router,
    data() {
        return {
            search: ''
        };
    },
    methods: {
        searchIt: _.debounce(() => {
            Fire.$emit('searching');
        }, 1000),
        searchNow() {
            Fire.$emit('searching');
        }
    }
});
