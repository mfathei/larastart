<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('user/search', 'Api\UserController@search');
Route::get('profile', 'Api\UserController@profile');
Route::patch('profile', 'Api\UserController@updateProfile');



// keep latest in this file as it overwrites the previous one user/search if u put this before it
Route::apiResources([
    'user' => 'Api\UserController',
]);
